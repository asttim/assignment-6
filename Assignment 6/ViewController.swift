//
//  ViewController.swift
//  Assignment 6
//
//  Created by Timur Astashov on 7/28/18.
//  Copyright © 2018 Timur Astashov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBAction func ActionButton(_ sender: UIButton) {
      summFunction()
    }
    
    @IBOutlet weak var TextField: UITextField!
    
    @IBOutlet weak var ResultLabel: UILabel!
    
    @IBOutlet weak var TextView: UITextView!
    
    var summInt: Int = 0
    var history: String = "History: "
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ResultLabel.text = "\(UserDefaults.standard.integer(forKey: "globalInt"))"
        TextView.text = "\(UserDefaults.standard.string(forKey: "string")!)"
    }
    
    func summFunction() {
        if TextView.text.contains(" ") || TextView.text.isEmpty || (TextField.text?.isEmpty)! {
           ResultLabel.text = "Error"
        } else {
            let textFromTextField = TextField.text!
            let numberFromTextView: Int? = Int(TextView.text!)
            let CGFloatForFontFromGlobalInt: CGFloat = CGFloat(summInt)
            let intFromTextFromField: Int? = Int(textFromTextField)
            print(textFromTextField)
            summInt = intFromTextFromField! + numberFromTextView!
            ResultLabel.text = "\(summInt)"
            UserDefaults.standard.set(summInt, forKey: "globalInt")
            TextView.font = TextView.font?.withSize(CGFloatForFontFromGlobalInt)
            history += "\(summInt)" + " "
            UserDefaults.standard.set(history, forKey: "string")
            print(UserDefaults.standard.string(forKey: "string")!)
        }
    }

}

